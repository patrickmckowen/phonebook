
# --------------------------------------------------------------------------- #
                               # IMPORTS #

require 'sinatra'
#require 'bcrypt'                # Uncomment when this is figured out
require_relative 'functions.rb'
enable 'sessions'


# ------------------------------------------------------------------------- #
                      # Instantiate Connections #
db_params = {
	host: ENV['contactbook_host'],
	port: ENV['contactbook_port'],
	dbname: ENV['contactbook_dbname'],
	user: ENV['contactbook_user'],
	password: ENV['contactbook_password']
}

db = db_init(db_params)


get '/' do
    erb :index
end


post '/handle_submission' do
    @params = params
    # SET SESSION VARIABLES
    session[:first_name] = params[:first_name]
    session[:last_name] = params[:last_name]
    session[:street_address] = params[:street_address]
    session[:city] = params[:city]
    session[:state] = params[:state]
    session[:zipcode] = params[:zipcode]
    session[:phone] = params[:phone]

    # INSERT The contact
    insert_contact(db, @params)

    # Redirect to page showing 
    # submited information
    redirect :show_submission
end



get '/show_submission' do
	erb :show_submission
end



get '/show_all_contacts' do
	erb :show_all_contacts, locals: {db: db}
end



post '/handle_update' do
    @params = params
    # CALL UPDATE FUNCTION WITH 
    # ALL THE FORM ELEMENTS
    update_contact(db, @params)

    # REDIRECT BACK TO PAGE THAT 
    # SHOWS ALL SUBMISSIONS
    redirect :show_all_contacts
end



get '/search_contacts' do
	erb :search_contacts
end



post '/search_contacts' do
	@params = params
	session[:search_table] = full_search_table_render(db, @params)
	redirect :search_contacts
end



get '/registration' do
	erb :registration
end



post '/registration' do
    handle_registration(
      params[:username],
      params[:password])
end



get '/login' do
	erb :login
end



post '/login' do
    handle_login(
      params[:username],
      params[:password])
end



get '/logout' do
    handle_logout()
    erb :login
end
