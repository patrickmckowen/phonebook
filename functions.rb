require 'pg'
require 'bcrypt'


def prepare_db_statements(db)
    
    db.prepare(
        'search_all',
        "SELECT * FROM contacts ")
        

    db.prepare( 
      'insert_query',

      
        %q( INSERT INTO contacts (
                first_name, last_name, street_address, 
                city, state, zipcode, phone) 
            VALUES (
                $1, $2, 
                $3, $4, 
                $5, $6, 
                $7);))
    db.prepare(
        'update_query',
         %q( UPDATE contacts
		SET first_name=$1,
		last_name=$2,
		street_address=$3,
		city=$4,
		state=$5,
	        zipcode=$6,
	        phone=$7
             WHERE id=$8;))



    db.prepare(
        'search_by_phone_number',
	%q(SELECT * FROM contacts
           WHERE phone=$1))

    db.prepare(
        'search_by_first_name_and_phone_number',
	%q(SELECT * FROM contacts 
           WHERE first_name=$1 
           AND phone=$2))
    
    db.prepare(
        'search_by_username',
	%q(SELECT * FROM contacts 
               WHERE username=$1))

    # insert the username and hashed password into the db
    db.prepare(
        'insert_user',
        %q(INSERT INTO contacts 
             (username, password)
          VALUES 
             ($1, $2);))
    db
end

        
def create_contacts_table(db) 
  db.exec(
    %q( CREATE TABLE IF NOT EXISTS contacts (
        id                SERIAL PRIMARY KEY,
        first_name        varchar(40),
        last_name         varchar(40),
        street_address    varchar(40), 
        city              varchar(40), 
        state             varchar(40), 
        zipcode           varchar(40), 
        phone             varchar(40),
        username          varchar(20),
        password          varchar(20));))
end

def db_init(db_params)
  db = PG::Connection.new(db_params)
  create_contacts_table(db)
  prepare_db_statements(db)
  db
end

def insert_contact(db, params)
    db.exec_prepared(
        'insert_query',
        [params['first_name'],
        params['last_name'], 
        params['street_address'], 
        params['city'], 
        params['state'], 
        params['zipcode'], 
        params['phone']])
end


def update_contact(db, params)
    db.exec_prepared(
        'update_query',
        [params[:first_name],
         params[:last_name], 
         params[:street_address], 
         params[:city], 
         params[:state], 
         params[:zipcode], 
         params[:phone],
         params[:id]])
end

        
def search_users(db, params)

    # INPUTS FROM SEARCH FORM
    first_name = params[:first_name]
    phone = params[:phone]

    # Input first name
    if first_name != '' && phone == ''
        db.exec_prepared('search_by_first_name', first_name)

    # Input phone number
    elsif first_name == '' && phone != ''
        db.exec_prepared('search_by_phone_number', phone)

    # Input first name and phone
    elsif first_name != '' && phone != ''
        db.exec_prepared('search_by_first_name_and_phone', first_name, phone)

    # Input neither first name nor phone
    else
        db.exec_prepared('search_all')
    end

end


def generate_table(response_obj)
    html = ''

    # Start table
    html << "<table>
	<tr>
	    <td>ID</td>
	    <td>First Name</td>
	    <td>Last Name</td>
	    <td>Street Address</td>
	    <td>City</td>
	    <td>State</td>
	    <td>Zipcode</td>
	    <td>Phone Number</td>
	  </tr>"

    # Row
    response_obj.each do |row|
        # Start row
        html << "<tr>"

        # Cells
        row.each do |cell| 
            html << "<td>#{cell[1]}</td>"
        end
        # End row
	html << "</tr>"
    end

    # End table
    html << "</table>"

    # Return string
    html
end


# TIE THEM ALL TOGETHER
def full_search_table_render(db, params)
    response_obj = search_users(params)
    generate_table(response_obj)
end


def delete_contact(db, id)
    db.prepare('delete_query', "DELETE FROM contacts WHERE ID=$1")
    db.execute_prepared('delete_query', id)
end


def entry_exists(db, col, val)

    rows = []

    db.prepare(
        'query',
        %q( SELECT * FROM contacts 
                WHERE $1=$2))

    response_obj = db.exec_prepared(col, val)

    response_obj.each do |row| 
        rows << row
    end
	
    rows.empty? == false
end


def get_password(db, username)


    resp = db.exec_query('search_by_username', username)

    resp[0]['password']
end


def handle_registration(db, username, password)
	# Check if the item is already in the database
	# If it's not in the db...
	if entry_exists(db, 'username', username) == false
	# hash the password
		hashed_pw = BCrypt::Password.create(password, :cost => 10)

                  db.exec_prepared(
                    'insert_user',
                    userame,
                    password)
		session[:username] = username
		redirect :show_all_contacts
	else
		"#{username} is already taken."
	end
end


def handle_login(db, username, password)
	# If the username is in the database...
	if entry_exists(db, 'username', username)
		# if the password is correct...
	  if BCrypt::Password.new(
               get_password(db, username)) == password
		# set the username as a session VARIABLE (can this be donw outside of the app.rb?)
		session[:username] = username
		redirect :show_all_contacts
		else
		"Password is incorrect"
		end
	else
		"#{username} does not exist. "
	end
end


def handle_logout()
	if session.key?('username')
		session.delete('username')
		redirect :login
	else
		"Not logged in"
	end
end


                           # INLINE TESTS #
# ----------------------------------------- #
# ----------------------------------------- #
#print(db.exec("SELECT * FROM contacts").class)
# ----------------------------------------- #
=begin
db.exec("SELECT * FROM contacts").each do |item|
	puts item.to_a
end
=end
# ----------------------------------------- #
=begin
db.exec("SELECT * FROM contacts").each do |item|
	item.each {|i| puts i}
end
=end
# ----------------------------------------- #

#print(prep_html(response_obj(prep_query({phone: '3044449994', first_name: ''}))))
#print(full_search_table_render({first_name: 'firsty', phone: '1-304-555-5555'}))

# db.exec("INSERT INTO contacts (first_name, last_name, street_address, city, state, zipcode, phone) VALUES ('Patrick', 'Mckowen', 'street', 'city', 'state', 'zipcode', 'phone');")

