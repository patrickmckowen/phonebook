
##############################################################################

class User

# -------------------------------------------------------------------------- #
  def initialize(username, password, first_name, last_name, street_address, city, state, zipcode, phone)
    @username = username
    @password = password
    @first_name = first_name
    @last_name = last_name
    @street_address = street_address
    @city = city
    @state = state
    @zipcode = zipcode                # Just adding but not giving option to give options on init
    @phone = phone
  end
# -------------------------------------------------------------------------- #
                         # SETTERS AND GETTERS #

# ---------------------------------- #
           # USERNAME #
# GET
def username()
  @username
end

# SET
def set_username(username)
  @username = username
end
# ---------------------------------- #
           # PASSWORD #
# GET
def password()
  @password
end

# SET
def set_password(password)
  @password = password
end
# ---------------------------------- #
           # FIRST NAME #
  # GET
  def first_name()
    @first_name
  end

  # SET
  def set_first_name(first_name)
    @first_name = first_name
  end
# ---------------------------------- #
           # LAST NAME #
# GET
def last_name()
  @last_name
end

# SET
def set_last_name(last_name)
  @last_name = last_name
end
# ---------------------------------- #
        # STREET ADDRESS #
# GET
def street_address()
  @street_address
end

# SET
def set_street_address(street_address)
  @street_address = street_address
end
# ---------------------------------- #
             # CITY #
# GET
def city()
  @city
end

# SET
def set_city(city)
  @city = city
end
# ---------------------------------- #
             # STATE #
# GET
def state()
  @state
end

# SET
def set_state(state)
  @state = state
end
# ---------------------------------- #
             # ZIPCODE #
# GET
def zipcode()
  @zipcode
end

# SET
def set_zipcode(zipcode)
  @zipcode = zipcode
end
# ---------------------------------- #
              # PHONE #
# GET
def phone()
  @phone
end

# SET
def set_phone(phone)
  @phone = phone
end
# ---------------------------------- #
# -------------------------------------------------------------------------- #
  def search_query()
    if @first_name && @phone
       "SELECT * FROM contacts WHERE first_name='#{@first_name}' AND phone='#{@phone}'"
	  elsif @first_name
		   "SELECT * FROM contacts WHERE first_name='#{@first_name}'"
    elsif @phone
		   "SELECT * FROM contacts WHERE phone='#{@phone}'"
	  else
		   "SELECT * FROM contacts"
    end
	end

# ------------------------------------------------------- #
  # Inserts full info
  def insert_query()
       "INSERT INTO users ('#{@username}', #{@password}', '#{@first_name}', '#{@last_name}', '#{@street_address}', '#{@city}', '#{@state}', '#{@zipcode}', '#{@phone}')"
  end
# ------------------------------------------------------- #
# This gets the initial login fiels of just
  def handle_login(params)
    # Step - 1: Check if the username exist already
    db.exec("SELECT * FROM contacts WHERE username='#{}'")
    # Step - 2: If it does, ajax return a message
    # Step - 2: If it does not exist, enter the username and hashed password and redirect to info page
end


#user1 = User.new('userSteve', '234kjhvoaasd879g', 'Steve', 'Stevenson', 'Steve Street', 'Steveville', 'Steveahoma', '12345', '1-304-555-3045')
#print(user1.insert_query(), "\n")
#print(user1.search_query(), "\n")
