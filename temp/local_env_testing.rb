# HOST
ENV['host'] = 'the host'
# PORT
ENV['port'] = 'the port'
# USERNAME
ENV['user'] = 'the user'
# PASSWORD
ENV['password'] = 'the password'
# DATABASE NAME
ENV['dbname'] = 'the database name'
