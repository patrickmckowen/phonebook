class Table
  def initialize(*columns)
    @columns = {}
    columns.each {|column| @columns.insert(column)}
  end

  def columns()
    @columns
  end

end

#table1 = Table.new(first_name: 'TEXT', andy: 2, paul: 3)
#print(table1.columns)
=begin
class Level_1
  def initialize(name, first_number)
    @name = name
    @first_number = first_number
  end
  def name
    @name
  end
end

class Level_2
  def initialize(level_1, second_number)
    @level_1 = level_1
    @second_number = second_number
  end

  def add_up2(level_1)
    @level_1.first_number + @second_number
  end
end

lvl1 = Level_1.new('Nameliness', 23)
lvl2 = Level_2.new(lvl1, 34)

print(lvl2.add_up2(lvl2), "\n")
=end

#require 'Bcrypt-Ruby'
#print BCrypt::Password.create('yorick', :cost => 10)
