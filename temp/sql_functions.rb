require 'pg'
load "./local_env.rb" if File.exists?("../local_env.rb")
db_params = {
	host: ENV['host'],
	port: ENV['port'],
	dbname: ENV['dbname'],
	user: ENV['user'],
	password: ENV['password']
}

db = PG::Connection.new(db_params)

# the db level holds the db configurations
class db < PG # Should be able to call the pg things from here
  # These CAN be set outside
	attr_accessor :host, :port, :dbname, :user, :password

  # Show active active databases
  def self.all
    ObjectSpace.each_object(self).to_a
  end

  # NUMBER OF TABLES IN MEMORY
  def self.count
    all.count
  end

  # INIT
	def initialize(host, port, dbname, user, password)
		@host = host
		@port = port
    @dbname = dbname
    @user = user
    @password = password
    @tables = []                # Just adding but not giving option to give options on init
	end

  # ADD TABLE
	def add_table(table)
		@tables << table
	end

  # Return all tables
	def get_tables
    return @orders
	end

end

class Table
	attr_accessor :name, :columns

	def self.all
    ObjectSpace.each_object(self).to_a
  end

  def self.count
    all.count
  end

	def initialize(name, **columns)
		@name = name                     #
    @rows = []                       #
    columns.each {|columns| @columns << columns}   #
	end

	def add_row(row)
		@rows << row
	end

	def get_rows()
    	return @rows.size
	end

	def total()
		self.pizzas.price.sum
	end
end

# A class of toppings will inherit from this
class Pizza_element
	attr_accessor :display_name, :code_name, :price, :picture, :input_type

	def self.all
    ObjectSpace.each_object(self).to_a
  end

  def self.count
    all.count
  end

	def initialize(display_name, code_name, price, picture, input_type)
		@display_name = display_name
		@code_name = code_name
    @price = price
    @picture = picture
    @input_type = input_type
	end



	def generate_input

		if self.input_type == 'radio'

			"<input type='#{@input_type}' name='#{@code_name}' value=''>"

		else

			"<input type='#{@input_type}' name='#{@code_name}'>"

		end

	end



end



class Topping < Pizza_element



	attr_accessor :category



	def initialize(display_name, code_name, price, picture, input_type, category)

		super(display_name, code_name, price, picture, input_type)

		@category = category

	end



end



class Pizza



	attr_accessor :size, :crust_type, :toppings



	def initialize(size, crust_type)

		@size = size

		@crust_type = crust_type

    	@toppings = []

	end



end
